const fs = require('fs')
const path = require('path')
const _ = require('lodash')

let env = _.indexOf(['pro', 'test'], process.env.NODE_ENV) === -1
    ? 'dev' : process.env.NODE_ENV

let config = {env}

let _config = fs.readdirSync(path.join(__dirname, env))
  .reduce((_config, filename) => {
    let key = filename.replace(/[.]/g, '_').replace(/_js$/, '')
    _config[key] = require(path.join(__dirname, env, filename))
    return _config
  }, {})

config = _.extend(config, _config)

module.exports = config
