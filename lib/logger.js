const log4js = require('log4js')
const config = require('../config')

let logConfig = config.log || {level: 'DEBUG'}
log4js.configure(logConfig.options)

module.exports = function getLogger (name) {
  let logger = log4js.getLogger(name)
  logger.setLevel(logConfig.level)

  return logger
}
