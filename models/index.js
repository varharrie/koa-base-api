const fs = require('fs')
const path = require('path')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = mongoose.Types.ObjectId // constructor function
const _ObjectId = Schema.Types.ObjectId // use in schema definitions

function capitalize (str) {
  return str.replace(/\w/, (w) => w.toUpperCase())
}

var models = {ObjectId}

fs.readdirSync(path.join(__dirname))
  .forEach((filename) => {
    if (filename.indexOf('index') > -1) return
    let modelName = capitalize(filename.replace(/.js$/, ''))
    models[modelName] = require(path.join(__dirname, filename))(mongoose, Schema, _ObjectId)
  })

module.exports = models
